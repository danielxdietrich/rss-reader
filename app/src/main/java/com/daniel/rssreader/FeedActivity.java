package com.daniel.rssreader;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;

import java.util.ArrayList;
import java.util.List;

public class FeedActivity extends AppCompatActivity {

    private List<String> listArticles = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;
    private SyndFeed feed = GlobalData.getFeed();

    private ListView listViewArticles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        getSupportActionBar().setTitle(feed.getTitle());
        GlobalData.setListArticles(listArticles);

        for (SyndEntry entry : GlobalData.getFeed().getEntries()) {
            listArticles.add(entry.getTitle());
        }

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listArticles);
        listViewArticles = findViewById(R.id.listViewArticles);
        listViewArticles.setAdapter(arrayAdapter);

        listViewArticles.setOnItemClickListener((adapterView, view, i, l) ->
        {
            Intent intent = new Intent(this, ArticleActivity.class);
            intent.putExtra("articleIndex", i);
            startActivity(intent);
        });
    }
}