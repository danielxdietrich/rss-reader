package com.daniel.rssreader;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rometools.rome.feed.synd.SyndEntry;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ArticleActivity extends AppCompatActivity {

    private String articleTitle;
    private SyndEntry syndEntry;
    private TextView textViewTitle, textViewDate;
    private WebView webViewArticle;
    private Button buttonNext, buttonPrev;

    private int articleIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        articleIndex = getIntent().getIntExtra("articleIndex", 0);
        articleTitle = GlobalData.getListArticles().get(articleIndex);

        getSupportActionBar().hide();

        textViewTitle = findViewById(R.id.textViewTitle);
        webViewArticle = findViewById(R.id.webViewArticle);
        textViewDate = findViewById(R.id.textViewDate);
        buttonNext = findViewById(R.id.buttonNext);
        buttonPrev = findViewById(R.id.buttonPrev);

        buttonNext.setOnClickListener(view -> openNextArticle());
        buttonPrev.setOnClickListener(view -> openPrevArticle());

        for (SyndEntry entry : GlobalData.getFeed().getEntries()) {
            if (entry.getTitle().equals(articleTitle)) {
                syndEntry = entry;
            }
        }

        String htmlContents = syndEntry
                .getContents()
                .get(0)
                .getValue();

        htmlContents = updateHtml(htmlContents);

        Date fullDate = new Date(syndEntry.getPublishedDate().getTime());
        String date = new SimpleDateFormat("dd/MM/yyyy").format(fullDate);

        textViewTitle.setText(syndEntry.getTitle());
        textViewDate.setText(syndEntry.getAuthor() + ", " + date);
        webViewArticle.loadDataWithBaseURL(null, htmlContents, "text/html", "utf-8", null);
    }

    private String updateHtml(String htmlContents) {
        String fontImport = " <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">\n" +
                "<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>\n" +
                "<link href=\"https://fonts.googleapis.com/css2?family=" + GlobalData.getFont().replace(" ", "+") + "&display=swap\" rel=\"stylesheet\"> ";

        String imageSetting = "img { display: inline; height: auto; max-width: 100%; }";
        String fontSetting = "p { font-family: '" + GlobalData.getFont() + "', sans-serif; }";
        String cssSettings = "<style>" + imageSetting + fontSetting + "</style>";

        if (!GlobalData.isHideImages()) {
            htmlContents = htmlContents.replaceAll("[<](/)?img[^>]*[>]", "");
        }

        htmlContents = cssSettings + fontImport + htmlContents;

        return htmlContents;
    }

    private void openNextArticle() {
        if (articleIndex + 1 < GlobalData.getListArticles().size()) {
            Intent intent = new Intent(this, ArticleActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            intent.putExtra("articleIndex", articleIndex + 1);
            startActivity(intent);
            finish();
        }
    }

    private void openPrevArticle() {
        if (articleIndex - 1 > -1) {
            Intent intent = new Intent(this, ArticleActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            intent.putExtra("articleIndex", articleIndex - 1);
            startActivity(intent);
            finish();
        }
    }
}