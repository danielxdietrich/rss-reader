package com.daniel.rssreader;

import com.rometools.rome.feed.synd.SyndFeed;

import java.util.List;

public class GlobalData {
    private static SyndFeed feed;
    private static int feedIndex;
    private static List<String> listArticles;

    private static boolean hideImages = true;
    private static String font = "Roboto";

    public static int getFeedIndex() {
        return feedIndex;
    }

    public static SyndFeed getFeed() {
        return feed;
    }

    public static void setFeedIndex(int feedIndex) {
        GlobalData.feedIndex = feedIndex;
        GlobalData.feed = MainActivity.FEED_LIST.get(feedIndex);
    }

    public static boolean isHideImages() {
        return hideImages;
    }

    public static void setHideImages(boolean hideImages) {
        GlobalData.hideImages = hideImages;
    }

    public static String getFont() {
        return font;
    }

    public static void setFont(String font) {
        GlobalData.font = font;
    }

    public static List<String> getListArticles() {
        return listArticles;
    }

    public static void setListArticles(List<String> listArticles) {
        GlobalData.listArticles = listArticles;
    }
}
